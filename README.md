# v2_mine

## 电脑端

 下载： https://cowtransfer.com/s/617bed7cc4b844

1. 打开 v2rayN.exe 主程序

<img src="./img/1.png" style="zoom:50%;" />

2. 开启 v2ray 后需要在状态栏单击图标打开主页面

![](./img/2.png)

3. 在主页面左上角选中“服务器”，选择“从剪贴板导入批量URL”，【需要先复制链接（vmess://那个）】

   <img src="./img/3.png" style="zoom:50%;" />

4. 在状态栏右下角右击图标，并选中服务器

<img src="./img/4.png" style="zoom:50%;" />

5. 开启 HTTPS 服务，选择“开启PAC,并自动配置PAC(PAC模式)”

<img src="./img/5.png" style="zoom:50%;" />

## 手机端

下载：https://cowtransfer.com/s/c0d743b412764e

1. 先复制链接（vmess://那个），打开 v2RayNG 软件，点击右上角的 “+” ，选择“从粘贴板导入订阅”

   <img src="./img/6.jpg" style="zoom:50%;" />

2. 先选中生成的节点，再点击右下角的 “v” 按钮，当状态栏出现钥匙图标即代表成功打开。

   如果是第一次使用VPN，手机会弹出一个关于VPN状态的提示，点击允许即可。

<img src="./img/7.jpg" style="zoom:50%;" />
